# README #


### What is this repository for? ###

Provides an R and C++ implementation of an ADMM algorithm for estimating high dimensional multivariate Hawkes processes with sparse
and low rank penalties. 
The details of the algorithm are provided in the paper "Learning Social Infectivity in Sparse Low-rank Networks Using
Multi-dimensional Hawkes Processes" by Zhou et al 2013.

