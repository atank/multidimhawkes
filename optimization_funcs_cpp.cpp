#include <Rcpp.h>
using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar


// [[Rcpp::export]]
void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}

// [[Rcpp::export]]
NumericMatrix calc_agp_cpp(int Nc, NumericMatrix Ap,NumericVector b,NumericMatrix Gm) {
  NumericMatrix agp(Nc,Nc);
  for (int i = 1; i < 6 ;i++) {
    for (int j = 0; j < i; j++) {
      int bi = b(i);
      int bj = b(j);
      //showValue(bi);
      //showValue(Ap(3,3));
      //showValue(Gm(i,j));
      agp(i,j) = Ap(bi,bj)*Gm(i,j);
    }
  }
  return(agp);
}

NumericMatrix calc_pc_cpp(NumericMatrix Ap, NumericVector mup, int N, int Nc, NumericVector b,NumericMatrix Gm) {
  NumericMatrix agp = calc_agp_cpp(Nc,Ap,b,Gm);
  NumericMatrix pc(Nc,Nc);
  for (int i = 0; i < Nc ; i++) {
    double smi = 0;
    for (int j = 0; j < i; j++) {
      smi += agp(i,j);
    }
    smi += mup(b(i));
    for (int j = 0; j < i; j++) {
      pc(i,j) = agp(i,j)/smi;
    }
    pc(i,i) = agp(i,i)/smi;
  }
  return(pc);
}
