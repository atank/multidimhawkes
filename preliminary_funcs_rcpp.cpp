#include <Rcpp.h>
using namespace Rcpp;


// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

//generate_gmat <- function(b,lam,Nc) {
//  Gm = matrix(0,Nc,Nc)
//  for (i in 2:Nc) {
//    for (j in 1:(i - 1))
//      Gm[i,j] = trigger_function(b[1,i] - b[1,j],lam)
//  }
//  return(Gm)
//}

// [[Rcpp::export]]
void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}

double trigger_function_cpp(double t,double lam,double tmax) {
  double out = 0;
  if (t < tmax) {    
    double outn = lam*exp( - t*lam );
    out = outn/(1 - exp(-lam*tmax));
  }
  if (t > tmax) {
    out = 0;
  }
   return(out);
}

// [[Rcpp::export]]
NumericMatrix generate_gmat_cpp(NumericVector b,double lam, int Nc,double tmax) {
  NumericMatrix y(Nc,Nc);
  for (int i = 1; i < Nc; i++) {
    for (int j = 0; j < i;j++) {
      double diff = b(i) - b(j);
      y(i,j) = trigger_function_cpp(diff,lam,tmax);
    }
  }
  return(y);
}


  
