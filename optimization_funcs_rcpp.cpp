#include <RcppArmadillo.h>
#include <Rcpp.h>
using namespace Rcpp;


// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar


// [[Rcpp::export]]
void showValue(double x) {
    Rcout << "The value is " << x << std::endl;
}

//double trigger_function_cpp(double t,double lam) {
//   double out = lam*exp( - t*lam );
//   return(out);
//}

// [[Rcpp::export]]
double logsumexp(NumericVector x) {
    return(log(sum(exp(x - max(x)))) + max(x));
}


double trigger_function_cpp(double t,double lam,double tmax) {
  double out = 0;
  if (t < tmax) {    
    double outn = lam*exp( - t*lam );
    out = outn/(1 - exp(-lam*tmax));
  }
  if (t > tmax) {
    out = 0;
  }
   return(out);
}

double cdf_trigger_function_cpp(double t, double lam, double tmax) {
  double out = 0;
  if (t < tmax) {
     double outn = 1 - exp(-lam*t);
     out = outn/(1 - exp(-lam*tmax));
  }
  if (t > tmax) {
    out = 1;
  }
  return(out);
}

// [[Rcpp::export]]
double compute_full_logliklihood_cpp(int N,int Nc,NumericVector btimes,NumericVector blab,int horizon, NumericMatrix A, NumericVector mu,double l1, double l2,double tmax) {
  double loglik = log(mu(blab(0)));
  showValue(loglik);
  for (int i = 1; i< Nc; i++) {
    double tot = mu(blab(i));
    for (int j = 0; j < i;j++) {
      double timediff = btimes(i) - btimes(j);
      tot = tot + A(blab(i),blab(j))*trigger_function_cpp(timediff,1,tmax);
    }
    if (tot == 0) {
      showValue(i);
    }
    loglik += log(tot);
    //showValue(loglik);
  }
  for (int i = 0; i < N; i++) {
    loglik -= horizon*mu(i);
  }
  for(int i = 0; i < N; i++) {
    for (int j = 0;j < Nc;j++) {
      loglik -= A(i,blab(j))*cdf_trigger_function_cpp(horizon-btimes(j),1,tmax);
    }
  }
  arma::mat As = as<arma::mat>(A);

  arma::vec s = svd(As);
  
  double a = sum(abs(s));
  double b = sum(sum(abs(As)));
  
  
  loglik -= l1*a + l2*b;
  return(-loglik);
}

double compute_full_logliklihood_EM_cpp_test(int N,int Nc,NumericVector btimes,NumericVector blab,int horizon, NumericMatrix A, NumericVector mu,double rho,arma::mat Z1,arma::mat Z2, arma::mat U1, arma::mat U2,double tmax) {
  double loglik = log(mu(blab(0)));
  for (int i = 1; i< Nc; i++) {
    
    NumericVector logsc(i+1);
    for (int j = 0; j < i;j++) {
      double timediff = btimes(i) - btimes(j);
      logsc(j) = log(A(blab(i),blab(j))) + log(trigger_function_cpp(timediff,1,tmax));
    }
    logsc(i)=log(mu(blab(i)));
    loglik += logsumexp(logsc);
  }
  for (int i = 0; i < N; i++) {
    loglik -= horizon*mu(i);
  }
  for(int i = 0; i < N; i++) {
    for (int j = 0;j < Nc;j++) {
      loglik -= A(i,blab(j))*cdf_trigger_function_cpp(horizon-btimes(j),1,tmax);
      //(1 - exp(-(horizon-btimes(j))));
      //cdf_trigger_function_cpp(horizon-btimes(j),1,tmax);
    }
  }
  arma::mat As = as<arma::mat>(A);
  double m1 = norm(As - Z1 + U1,"fro");
  double m2 = norm(As - Z2 + U2,"fro");

    
  loglik -= (rho/2)*(m1*m1 + m2*m2);
  return(-loglik);
}


double compute_full_logliklihood_EM_cpp(int N,int Nc,NumericVector btimes,NumericVector blab,int horizon, NumericMatrix A, NumericVector mu,double rho,arma::mat Z1,arma::mat Z2, arma::mat U1, arma::mat U2,double tmax) {
  double loglik = log(mu(blab(0)));
  for (int i = 1; i< Nc; i++) {
    double tot = mu(blab(i));
    for (int j = 0; j < i;j++) {
      double timediff = btimes(i) - btimes(j);
      tot = tot + A(blab(i),blab(j))*trigger_function_cpp(timediff,1,tmax);
    }
    loglik += log(tot);
  }
  for (int i = 0; i < N; i++) {
    loglik -= horizon*mu(i);
  }
  for(int i = 0; i < N; i++) {
    for (int j = 0;j < Nc;j++) {
      loglik -= A(i,blab(j))*cdf_trigger_function_cpp(horizon-btimes(j),1,tmax);
      //(1 - exp(-(horizon-btimes(j))));
      //cdf_trigger_function_cpp(horizon-btimes(j),1,tmax);
    }
  }
  arma::mat As = as<arma::mat>(A);
  double m1 = norm(As - Z1 + U1,"fro");
  double m2 = norm(As - Z2 + U2,"fro");

    
  loglik -= (rho/2)*(m1*m1 + m2*m2);
  return(-loglik);
}

//NumericMatrix <- soft_threshold(NumericMatrix M,double thresh,int N) {
//  for (int i = 0; i < N;i++) {
//    for (int j = 0; j < N; j++) {
//      abs(sign)
//    }
//  }
//}

//soft_threshold <- function(A,l2,rho,U2) {
//  sumau = A + U2
//  signmat = sign(sumau)
//  Z2 = sumau - signmat*l2/rho
//  thresh = (abs(sumau) > l2/rho)
//  Z2 = Z2*thresh
//  return(Z2)
//}

// [[Rcpp::depends(RcppArmadillo)]]
//[[Rcpp::export]]
arma::mat calc_b_cpp(arma::mat B,int N, int opt_int,arma::mat Z1, arma::mat Z2, arma::mat U1, arma::mat U2,double rho) {
  if (opt_int == 1) {
    return(B);
  }
  if (opt_int == 2) {
    //showValue(1);
    return(B + rho*(-Z1 + U1 - Z2 + U2));
  }
}




// [[Rcpp::export]]
NumericMatrix calc_agp_cpp(int Nc, NumericMatrix Ap,NumericVector b,NumericMatrix Gm,NumericVector lag_start) {
  NumericMatrix agp(Nc,Nc);
  for (int i = 1; i < Nc ;i++) {
    for (int j = lag_start(i); j < i; j++) {
      int bi = b(i);
      int bj = b(j);
      //showValue(bi);
      //showValue(Ap(3,3));
      //showValue(Gm(i,j));
      agp(i,j) = Ap(bi,bj)*Gm(i,j);
    }
  }
  return(agp);
}

// [[Rcpp::export]]
NumericMatrix calc_pc_cpp(NumericMatrix Ap, NumericVector mup, int N, int Nc, NumericVector b,NumericMatrix Gm,NumericVector lag_start) {
  NumericMatrix agp = calc_agp_cpp(Nc,Ap,b,Gm,lag_start);
  NumericMatrix pc(Nc,Nc);
  for (int i = 0; i < Nc ; i++) {
    double smi = 0;
    for (int j = lag_start(i); j < i; j++) {
      smi += agp(i,j);
    }
    smi += mup(b(i));
    for (int j = lag_start(i); j < i; j++) {
      pc(i,j) = agp(i,j)/smi;
    }
    pc(i,i) = mup(b(i))/smi;
  }
  return(pc);
}



// [[Rcpp::export]]
NumericMatrix calc_c_cpp_fornw_TEST(NumericMatrix pc, int N,NumericVector lag_start,NumericVector b,int Nc) {
  NumericMatrix c(N,N);
  for (int i = 1; i < Nc; i++) {
    for (int j = lag_start(i);j < i;j++) {
      int ui = b(i);
      int uj = b(j);
      //showValue(ui);
      //showValue(pc(i,j));
      c(ui,uj) += pc(i,j);
    }
  }
  return(c);
}


// [[Rcpp::export]]
arma::mat update_A_cpp_TEST(NumericMatrix pc, arma::mat B,int N, double rho, arma::mat Z1, arma::mat Z2, arma::mat U1, arma::mat U2, int opt_int, NumericVector time_lag, NumericVector b,int Nc) {
  arma::mat Bu = calc_b_cpp(B, N, opt_int, Z1, Z2, U1, U2, rho);
  //showValue(Bu(1,1));
  //NumericMatrix Cn = calc_c_cpp_while(event_orders,pc,N);
  NumericMatrix Cn = calc_c_cpp_fornw_TEST(pc,N,time_lag,b,Nc);
  arma::mat C = as<arma::mat>(Cn);
  if (opt_int == 1){
    return(C/Bu);
  }
  else {
    return((-Bu + sqrt(Bu % Bu + 8*rho*C))/(4*rho));
  }
}

// [[Rcpp::export]]
NumericVector update_mu_cpp_for(int N, int Nc, NumericMatrix pc,NumericVector blab,int horizon) {
  NumericVector mu_up(N);
  for (int i = 0; i < Nc; i++) {
    int ind = blab(i);
    mu_up(ind) += pc(i,i);
  }
  for (int j = 0; j < N;j++) {
    mu_up(j) = mu_up(j)/horizon;
  }
  return(mu_up);
}



// [[Rcpp::export]]
List optim_hawkes_Ab_cpp_TEST(int N, int Nc, NumericVector blab, NumericMatrix Gm, arma::mat B, int horizon, int N_opt, 
arma::mat Z1, arma::mat Z2, arma::mat U1, arma::mat U2, NumericMatrix Ap, NumericVector mup, double rho, int opt_int, NumericVector btimes,NumericVector lag_start,double tmax) {
  NumericVector loglik(N_opt);
  for (int i = 0;i < N_opt;i++) {
    //loglik[i] = compute_full_logliklihood_EM_cpp_test(N,Nc,btimes,blab,horizon,Ap,mup,rho,Z1,Z2,U1,U2,tmax);
    NumericMatrix pc = calc_pc_cpp(Ap,mup,N,Nc,blab,Gm,lag_start);
    mup =  update_mu_cpp_for(N, Nc, pc,blab,horizon);
    arma::mat as = update_A_cpp_TEST(pc, B,N, rho, Z1, Z2, U1, U2, opt_int, lag_start,blab,Nc);
    Ap = wrap(as);
  }
  return(List::create(mup,Ap,loglik));
}


//NumericMatrix calc_c_cpp(List event_orders,NumericMatrix pc, Numeric rel_events,int N) {
//  NumericVector vecArray(rel_events) 
//  Rccp::List event_ordersc(event_orders);
//  NumericMatrix C(N,N);
//  for (u = 0; u < N; u++) {
//    SEXP llu = event_ordersc[u]
//    NumericVector yu(llu);
//    int nu = yu.size();
//    for (up = 0; up < N; up++) {
//      SEXP llup = event_ordersc[up]
//      NumericVector yu(llup);
//      for (i = 0; i < nu; i++) {
//        int place = yu(i);
//        int maxev = rel_events(u,up,place);
//        for (j = 0; j < maxev; j++) {
//          C[u,up] += pc(place, yu(j))
//        }
//      }
//    }
//  }
//}

//calc_c <- function(event_orders,pc,rel_events,N) {
//  C = matrix(0,N,N)
//  for (u in 1:N) {
//    for (up in 1:N) {
//      #print(event_order[[u]])
//      for (i in event_orders[[u]]) {
//        #relevant_events = event_orders[[up]][which(event_orders[[up]] < i)]
//        relevant_events = event_orders[[up]][1:rel_events[u,up,i]]
//        #print(relevant_eventsum)
//        #print(relevant_events)
//        #print(relevant_events)
//        C[u,up] = C[u,up] + sum(pc[i,relevant_events])
//      }
//    }
//  }
//  return(C)
//}

//arma::mat update_A_cpp(arma::sp_mat pc, List event_orders,int horizon, arma::mat B, int N,double rho, arma::mat Z1,arma::mat Z2,arma::mat U1,arma::mat U2,int opt_int,NumericVector time_lag,NumericVector b,int Nc) {
//  arma::mat Bu = calc_b_cpp(B, N, opt_int, Z1, Z2, U1, U2, rho);
  //NumericMatrix Cn = calc_c_cpp_while(event_orders,pc,N);
//  NumericMatrix Cn = calc_c_cpp_fornw(pc,N,time_lag,b,Nc);
//  arma::mat C = as<arma::mat>(Cn);
//  if (opt_int == 1){
//    return(C/Bu);
//  }
//  else {
//    return((-Bu + sqrt(Bu % Bu + 8*rho*C))/(4*rho));
//  }
//}


//NumericVector update_mu_cpp(int N,arma::sp_mat pc, List event_orders, int horizon) {
////NumericVector update_mu_cpp(int N,NumericMatrix pc, List event_orders, int horizon) {
//  List event_ordersc(event_orders);
//  NumericVector mu_up(N);
//  //showValue(3);
//  for (int i = 0; i < N; i++) {
//    SEXP ll = event_ordersc(i);
//    NumericVector events(ll);
//    int n = events.size();
//    mu_up(i) = 0;
//    for (int j = 0; j < n; j++) {
//      //showValue(events(j));
//      mu_up(i) += pc(events(j) - 1,events(j) - 1);
//    }
//    mu_up(i) = mu_up(i)/horizon;
//  }
//  return(mu_up);
//}


//arma::sp_mat calc_agp_cpp_new(int Nc, NumericMatrix Ap,NumericVector b,NumericMatrix Gm,NumericVector time_lag,int total_back) {
//  NumericMatrix agp(Nc,Nc);
//  arma::colvec val(total_back);
//  arma::umat loc(2,total_back);
//  int counter = 0;
//  for (int i = 1; i < Nc ;i++) {
//    for (int j = time_lag[i]; j < i; j++) {
//      loc(0,counter) = i;
//      loc(1,counter) = j;
//      val(counter) = Ap(b(i),b(j))*Gm(i,j);
//      agp(i,j) = Ap(b(i),b(j))*Gm(i,j);
//      counter++;
//    }
//  }
//  arma::sp_mat agp_s(loc,val);
//  return(agp_s);
//}

//NumericMatrix calc_c_cpp_fornw(arma::sp_mat pc, int N,NumericVector time_lag,NumericVector b,int Nc) {
//  NumericMatrix c(N,N);
//  for (int i = 0; i < Nc; i++) {
//    for (int j = time_lag(i);j < i;j++) {
//      int ui = b(i);
//      int uj = b(j);
      //showValue(ui);
      //showValue(pc(i,j));
//      c(ui,uj) += pc(i,j);
//    }
//  }
//  return(c);
//}

//NumericMatrix
// arma::sp_mat calc_pc_cpp_new(NumericMatrix Ap, NumericVector mup, int N, int Nc, NumericVector b, NumericMatrix Gm, NumericVector time_lag,int total_back) {
//  arma::sp_mat agp = calc_agp_cpp_new(Nc,Ap,b,Gm,time_lag,total_back);
//  NumericMatrix pc(Nc,Nc);
//  arma::colvec val(total_back+Nc);
//  arma::umat loc(2,total_back+Nc);
  //showValue(total_back+Nc);
//  int counter = 0;
//  for (int i = 0; i < Nc ; i++) {
    //showValue(i);
//    double smi = 0;
//    for (int j = time_lag[i]; j < i; j++) {
//      smi += agp(i,j);
//    }
//    smi += mup(b(i));
//    for (int j = time_lag[i]; j < i; j++) {
      //pc(i,j) = agp(i,j)/smi;
//      loc(0,counter) = i;
//      loc(1,counter) = j;
//      val(counter) = agp(i,j)/smi;
//      counter++;
//    }
    //pc(i,i) = mup(b(i))/smi;
//    loc(0,counter) = i;
//    loc(1,counter) = i;
//    val(counter) = mup(b(i))/smi;
//    counter++;
//  }
  //showValue(counter);
  //showValue(1);
//  arma::sp_mat pc_s(loc,val);
  //return(pc);
//  return(pc_s);
//}


//arma::sp_mat calc_agp_cpp_new(int Nc, NumericMatrix Ap,NumericVector b,NumericMatrix Gm,NumericVector time_lag,int total_back) {
//  NumericMatrix agp(Nc,Nc);
//  arma::colvec val(total_back);
//  arma::umat loc(2,total_back);
//  int counter = 0;
//  for (int i = 1; i < Nc ;i++) {
//    for (int j = time_lag[i]; j < i; j++) {
//      loc(0,counter) = i;
//      loc(1,counter) = j;
//      val(counter) = Ap(b(i),b(j))*Gm(i,j);
//      agp(i,j) = Ap(b(i),b(j))*Gm(i,j);
//      counter++;
//    }
//  }
//  arma::sp_mat agp_s(loc,val);
//  return(agp_s);
//}
