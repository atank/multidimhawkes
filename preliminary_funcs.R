
calc_event_order <- function(b) {
  event_orders = list()
  for (i in 1:N) {
    event_orders[[i]] = which(b[2,] == i)
  }
  return(event_orders)
}

calc_lagstart <- function(b,timelag, Nc) {
  lagstart = rep(0,Nc)
  lagstart[1] = 1
  for (i in 2:Nc) {
    j = i - 1
    timediff = b[1,i] - b[1,j]
    while (j >= 1 & timediff < timelag) {
      j = j - 1
      if (j > 0) { 
        timediff = b[1,i] - b[1,j]      
      }
    }
    lagstart[i] = j + 1
  }
  return(lagstart)
}

calc_rel_events <- function(N,Nc,event_orders) {
  rel_events = array(0,c(N,N,Nc))
  for (u in 1:N) {
    for (up in 1:N) {
      for (i in event_orders[[u]]) {
        elv = which(event_orders[[up]] < i)
        if (length(elv) == 0) {
          rel_events[u,up,i] = 0
        }
        else {
          rel_events[u,up,i] = max(elv)
        }
      }
    }
  }
  return(rel_events)
}


order_hawkes <- function(h,N) {
  order = c()
  ass = c()
  for (i in 1:N) {
    #print(h[[1]][[i]])
    mN = length(h[[1]][[i]])
    if (mN > 0) {
      order = c(order, h[[1]][[i]])
      ass = c(ass, rep(i,mN))
    }
  }
  b = rbind(order,ass)
  b = b[,order(b[1,])]
  return(b)
}


calc_b_first_part <- function(horizon,hh,lam,N) {
  B = rep(0,N)
  for (i in 1:N) {
    B[i] = sum(CDF_trigger_function(horizon - hh[[i]],lam))
  }
  return(B)
}


calc_thinned_times <- function(event_orders,N,Nc,hhold) {
  hh = list()
  for (i in 1:N) {
    keep = which(event_orders[[i]] <= Nc)
    hh[[i]] = hhold[[i]][keep]
  }
  return(hh)
}

generate_gmat <- function(b,lam,Nc) {
  Gm = matrix(0,Nc,Nc)
  for (i in 2:Nc) {
    for (j in 1:(i - 1))
      Gm[i,j] = trigger_function(b[1,i] - b[1,j],lam)
  }
  return(Gm)
}

CDF_trigger_function <- function(t, lam) {
  return(1 - exp(-t*lam))
}

trigger_function <- function(t,lam) {
  return(lam*exp(-t*lam))
}

compute_rel_err <- function(A,Aest,N) {
  tot = 0
  for (i in 1:N) {
    for (j in 1:N) {
      if (A[i,j] == 0) {
        tot = tot + abs(A[i,j] - Aest[i,j]) 
      }
      else {
        tot = tot + abs(A[i,j] - Aest[i,j])/A[i,j]
      }
    }
  }
  return(tot/(N^2))
}

compute_avg_rank_corr <- function(A,Aest,N) {
  tot = 0
  for (i in 1:N) {
    if (sd(Aest[,i]) == 0) {}
    else {
      tot = tot + cor(A[,i],Aest[,i],method="kendall")
    }
  }
  return(tot/N)
}

compute_roc <- function(A,Aest) {
  areNzero = which(A != 0);
  arezero = which(A == 0)
  Amax = max(Aest)
  steps = unique(sort(as.vector(Aest)))
  steps = c(steps, max(steps) + 100)
  roc = matrix(0,2,length(steps))
  for (i in 1:length(steps)) {
    #print(i)
    nozero = which(Aest >= steps[i])
    zero = which(Aest < steps[i])

    roc[1,i] = length(intersect(nozero,areNzero))/length(areNzero)
    roc[2,i] = length(intersect(nozero,arezero))/length(arezero)
  }
  
  ###compute area under the ROC curve 
  auc = trapz(rev(roc[2,]),rev(roc[1,]))
  return(list(roc,auc))
}

calc_N_per_set <- function(hh,N) {
  np = rep(0,N)
  for (i in 1:N) {
    np[i] = length(hh[[i]])
  }
  return(list(min(np),np))
} 
